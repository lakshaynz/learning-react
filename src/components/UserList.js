import React, { useState, useEffect } from 'react'

function UserList() {
    const [user, SetUser] = useState([])

    useEffect(() => {
        fetch('https://jsonplaceholder.typicode.com/users')
            .then(resp => resp.json())
            .then(result => {
                console.log(result)
                SetUser(result)
            })
    }, [])

    return (
        <div>
            <h1 className='User_list_title'>Users Business cards</h1>
            <div className='business_card_container'>
                {
                    user.map((ele) => (
                        <div className='business_card'>
                            <h3 >{ele.name} </h3>
                            <h4>{ele.email} </h4>
                            <h4>{ele.phone} </h4>
                        </div>
                    )
                    )
                }
            </div>
        </div>
    )
}

export default UserList