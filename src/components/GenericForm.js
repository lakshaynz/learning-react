import React, { useState } from 'react'

function translateToChinese(english_input){
    let number_of_chars = english_input.length

    return "当我在这里输入中文时，我想在下一个看到翻译".substring(0,number_of_chars)
}


function translateToPinyin (english_input){
    let number_of_chars = english_input.length
    return "Dāng wǒ zài zhèlǐ shūrù zhōngwén shí, wǒ xiǎng zàixià yīgè kàn dào fānyì".substring(0,number_of_chars)
}

function GenericForm() {
    const [input_text, SetInputText] = useState('')
    const [select_val, SetSelectVal] = useState('1')
    const [radio_val, SetRadioVal] = useState('')
    const [checkbox_bool, SetCheckboxBool] = useState(false)
    return (
        <div>
            <input type='text' className='english_input' value={input_text} onChange={(e) => SetInputText(e.target.value)} />
            <h1>{translateToChinese(input_text)}</h1>
            <h1>{translateToPinyin(input_text)}</h1>
            {/* <h1> selected  is {select_val}</h1>
            <h1> radio   is {radio_val}</h1> */}

            {/* <select value={select_val} onChange={(e) => SetSelectVal(e.target.value)}>
                <option value='1'>Option1 </option>
                <option value='2'>Option2 </option>
                <option value='3'>Option3</option>
            </select>

            <input type="radio" onChange={(e) => SetRadioVal(e.target.value)} value='male' checked={radio_val === 'male'} /> male

            <input type="radio" onChange={(e) => SetRadioVal(e.target.value)} value='female' checked={radio_val === 'female'} /> Female

            <input type="radio" onChange={(e) => SetRadioVal(e.target.value)} value='other' checked={radio_val === 'other'} /> other */}

    
            {/* <input type='checkbox' onChange={(e)=>SetCheckboxBool(e.target.checked)} checked={checkbox_bool}  /> */}


        </div>
    )
}

export default GenericForm