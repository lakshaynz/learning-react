import React, { useState } from "react";

function FacebookSignUp() {

  const [firstName, SetFirstName] = useState("");
  const [surname, SetSurname] = useState("");
  const [mobileNumber, SetMobileNumber] = useState("");
  const [password, SetPassword] = useState("");

  const [date, SetDate] = useState("1");
  const [month, SetMonth] = useState("1");
  const [year, SetYear] = useState("2");
  const [gender, SetGender] = useState("");

  const theObject = {
    firstName,
    surname,
    mobileNumber,
    password,
    date,
    month,
    year,
    gender
  }
  function showAllValues() {
    console.log(theObject)
  }

  return (
    <div>
      <div className="fb-container">
        <div className="fb-container-child">
          l
          <div className="title_container">
            <div>
              <h1>Sign Up</h1>
              <h4>It's quick and easy.</h4>
            </div>
            <div className="close">X</div>
          </div>
          <div className="the_form">
            <div className="name_container">
              <input
                value={firstName}
                onChange={(e) => SetFirstName(e.target.value)}
                placeholder="First Name"
                className="name_input"
                type="text"
              />
              <input
                value={surname}
                onChange={(e) => SetSurname(e.target.value)}
                placeholder="Surname"
                className="name_input last_name"
                type="text"
              />
            </div>
            <input
              value={mobileNumber}
              onChange={(e) => SetMobileNumber(e.target.value)}
              placeholder="Mobile Number"
              className="other_info_input email_or_phone_input"
              type="text"
            />
            <input
              value={password}
              onChange={(e) => SetPassword(e.target.value)}
              placeholder="Password"
              type="password"
              className="other_info_input password_input"
            />
            <p>Date of birth</p>
            <div className="date_of_birth">
              <select
                value={date}
                onChange={(e) => SetDate(e.target.value)}
                className="dob_select"
              >
                <option value="1"> 1 </option>
                <option value="2"> 2 </option>
                <option value="3"> 3 </option>
                <option value="4"> 4 </option>
                <option value="5"> 5 </option>
              </select>
              <select
                value={month}
                onChange={(e) => SetMonth(e.target.value)}
                className="dob_select"
              >
                <option value="1">Jan</option>
                <option value="2"> FEB</option>
                <option value="3"> MAR </option>
                <option value="4"> APR</option>
                <option value="5">MAy</option>
              </select>
              <select
                value={year}
                onChange={(e) => SetYear(e.target.value)}
                className="dob_select"
              >
                <option value="2001"> 2001 </option>
                <option value="2002"> 2002 </option>
                <option value="2003"> 2003 </option>
                <option value="2004"> 2004 </option>
                <option value="2005"> 2005 </option>
              </select>
            </div>
            <div className="gender_div">
              <p>gender</p>
              <div className="gender_wrapper">
                <div>
                  Female
                  <input
                    value="Female"
                    type="radio"
                    checked={gender === "Female"}
                    onChange={(e) => SetGender(e.target.value)}
                    className="gender_radio"
                  />
                </div>
                <div>
                  Male
                  <input
                    type="radio"
                    value="Male"
                    checked={gender === "Male"}
                    onChange={(e) => SetGender(e.target.value)}
                    className="gender_radio"
                  />
                </div>
                <div>
                  Other
                  <input
                    type="radio"
                    value="Other"
                    checked={gender === "Other"}
                    onChange={(e) => SetGender(e.target.value)}
                    className="gender_radio"
                  />
                </div>
              </div>
            </div>
            <div className="tos_div">
              <p>
                People who use our service may have uploaded your contact
                information to Facebook. Learn more.By clicking Sign Up, you
                agree to our Terms, Privacy Policy and Cookies Policy. <br />
                You may receive SMS notifications from us and can opt out at any
                time.
              </p>
            </div>
            <div className="signup_button_div">
              <button onClick={showAllValues}>Signup</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default FacebookSignUp;
