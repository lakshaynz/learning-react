import React from 'react'

function DellCard(props) {

    return (
        <div className='container'>
            <div className='laptop'>
                <h2> {props.title}  </h2>
                <p>
                    {props.description}
                </p>
                <button>{props.button_text} </button>
            </div>
            

        </div>

    )
}

export default DellCard