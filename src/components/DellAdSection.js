import React from 'react';


function DellAdSection({title, subTitle, desc, button_text_1,button_text_2,bg_color,image}) {
  return (
    <div className={`container1 color-${bg_color}`}>
        <div className='container2'>
            <h1>{title}</h1>
            <h3>{subTitle}</h3>
            <p>{desc}</p>
            <button className='dell_sec_button_1'>{button_text_1}</button>
            <button className='dell_sec_button_2'>{button_text_2}</button>

        </div>
        <div className='dell_section_img'>
            <img alt=''  src={image}  />
        </div>

    </div>
  )
}

export default DellAdSection