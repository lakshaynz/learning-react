
import './App.css';

import DellCard from './components/DellCard';
import DellAdSection from './components/DellAdSection';
import img1 from './assets/images/one.avif'
import img2 from './assets/images/two.avif'
import img3 from './assets/images/three.avif'
import UserList from './components/UserList';
import GenericForm from './components/GenericForm';
import FacebookSignUp from './components/FacebookSignUp';

function App() {

  return (
    <div className="App">
      <FacebookSignUp />
      {/* <GenericForm /> */}
      {/* <UserList />
      <div className='container'>
        <DellCard
          title='XPS'
          description='High-performance multitasking  Photography and music production  Digital content creation  Sleek mobility'
          button_text='Shop XPS'
        />
        <DellCard
          title='Latitude'
          description='High-performance multitasking  Photography and music production  Digital content creation  Sleek mobility'
          button_text='Shop Latitude'
        />
        <DellCard
          title='Insperion'
          description='High-performance multitasking  Photography and music production  Digital content creation  Sleek mobility'
          button_text='Shop Insperion'
        />
      </div>
      <br />
      <DellAdSection title='Latitude Laptops & 2-in-1s'
        subTitle='For Small Business Productivity'
        desc='Laptops with the right balance of business features and outstanding support for small business.'
        button_text_1='Shop Vostro'
        button_text_2='Shop 2 '
        bg_color='white'
        image={img1}
      />
      <DellAdSection title='Insperion Laptops & 2-in-1s'
        subTitle='For Small Business Productivity'
        desc='Laptops with the right balance of business features and outstanding support for small business.'
        button_text_1='Shop Vostro'
        button_text_2='Shop 2 '
        bg_color='gray'
        image={img2}
      />
      <DellAdSection title='Vostro Small Business Laptops'
        subTitle='For Small Business Productivity'
        desc='Laptops with the right balance of business features and outstanding support for small business.'
        button_text_1='Shop Vostro'
        button_text_2='Shop 2 '
        bg_color='white'
        image={img3}
      /> */}
    </div>
  );
}

export default App;
